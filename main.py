from bs4 import BeautifulSoup as bs
import re
from os import path
import requests
import codecs

header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate',
        'Referer': 'https://www.google.com/'
        }


def entry_cleanup(entry):
    if entry.string:
        entry =  entry.string.strip()
    elif entry.a:
        a_tags = [entry_cleanup(x) for x in entry.findAll('a')]
        entry = a_tags[0] if len(a_tags) == 1 else a_tags
    elif entry.img:
        img_tags = [entry_cleanup(x) for x in entry.findAll('img')]
        entry = img_tags[0] if len(img_tags) == 1 else [x.src for x in img_tags]
    elif type(entry) == type(list()):
        temp = [entry_cleanup(x) for x in entry]
        entry = temp[0] if len(a_tags) == 1 else temp
    elif entry.text:
        entry =  entry.text.strip()
    return entry


def dex_by_num(num):
    base = 'https://www.serebii.net/pokedex-swsh/'
    tags = dex_entries[num].findAll('td')
    num = entry_cleanup(tags[0])
    name = pattern.sub('',entry_cleanup(tags[3]))
    url = base + name.lower().replace(' ','')
    entry = bs(requests.get(url,headers=header).text, features='html5lib')
    loc = entry.find(lambda x: x.name == "h2" and "Location" in x.text).parent.parent.parent
    clean_loc = [x.strip() for x in loc.text.split('\n') if x.strip()]
    detrash = [line[:-len('Details')] if line.endswith('Details') else line for line in clean_loc]
    formated_how_to_get = '"{}": "{{{}}}", "{}": "{{{}}}"'.format(detrash[1], detrash[2], detrash[3], detrash[4]) 
    result = ('[ ] {}: [{}]({}) {{{}}}'.format(num,name,url,formated_how_to_get))
    return result


def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False


def valid_dex_num(i):
    i = int(i)
    return i < 401 or i<0


def write_to_file(text, f='missing_dex.md'):
    with open(f, 'a') as md:
        md.write(text + '\n')


def print_prog_bar(cur,maxim, pre="Progress"):
    workdone = cur/maxim
    print("\r{0}: [{1:50s}] {2:.1f}%".format(pre,'#' * int(workdone * 50), workdone*100), end="", flush=True)


# TODO: Pokemon Class
# TODO: Search by name
# TODO: DexEntry caching
if __name__ == '__main__':
    if not path.exists("galardex.html"):
        dex_url = 'https://www.serebii.net/swordshield/galarpokedex.shtml'
        response = requests.get(dex_url, headers=header)
        open('galardex.html', 'w').write(response.text)
    dex = bs(codecs.open('galardex.html', 'r', encoding='utf-8',errors='ignore').read(), features='html5lib')
    dex_table = dex.findAll('table')[1] # The 2nd table on the website contains the Pokedex
    dex_entries = list(dex_table.findAll('tr')[1:][0].next_siblings) # First 2 rows are headers
    pattern = re.compile('[^a-zA-Z_\.\s\-\':]+')
    while(True):
        user = input('Dex-Nr: ')
        if is_int(user):
            user = int(user)
            if not valid_dex_num(user):
                continue
            result = dex_by_num(user)
            print(result[4:])
            write_to_file(result)
        else:
            a = [x.strip() for x in user.split(',')]
            for h,i in enumerate(a):
                i = int(i)
                if not valid_dex_num(i):
                    continue
                result = dex_by_num(i)
                write_to_file(result)
                print_prog_bar(h,len(a)-1,pre="#{}".format(i))
                    

    # abilities = entry_cleanup(tags[4])
    # typ = entry_cleanup(tags[5])
    # hp = entry_cleanup(tags[6])
    # attack = entry_cleanup(tags[7])
    # defense = entry_cleanup(tags[8])
    # sattack = entry_cleanup(tags[9])
    # sdefense = entry_cleanup(tags[10])
    # speed = entry_cleanup(tags[11])
    # print('abilities: ', abilities)
    # print('typ: ', typ)
    # print('hp: ', hp)
    # print('attack: ', attack)
    # print('defense: ', defense)
    # print('sattack: ', sattack)
    # print('sdefense: ', sdefense)
    # print('speed: ', speed)
