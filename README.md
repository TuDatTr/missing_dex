# missing_dex

Webscraper for serebii.net to look for locations of Pokemon I'm missing in the Galar-Dex


## How to install

First off you'll need Python3.

After installing it and adding it to your path (for windows users: This is an
option during the installation process), open your console in the directory the project
is in and execute `pip install -r requirements.txt` (For windows users it's
`py -m pip install -r requirements.txt`).
This should install all necessary packages.

## Examples

There are 2 ways to increment your list of missing pokemon.

Example 1:

```sh
Dex-Nr: 2
#002: [Thwackey](https://www.serebii.net/pokedex-swsh/thwackey) {"Sword": "{Evolve Grookey}", "Shield": "{Evolve Grookey}"}
```

Example 2:

```sh
Dex-Nr: 3,4,5,6
#6: [##################################################] 100.0%
```

## Todos

Many todos
